window.onload = () => {

const connectButton = document.getElementById('connectButton');
const disconnectButton = document.getElementById('disconnectButton');
const connectionState = document.getElementById('connectionState');
const deviceState = document.getElementById('deviceState');

const queryButton = document.getElementById('queryButton');

const stopChannelButton = document.getElementById('stopChannelButton');
const stopChannelNum = document.getElementById('stopChannelNum');

const adjustChannelButton = document.getElementById('adjustChannelButton');
const adjustChannelNum = document.getElementById('adjustChannelNum');
const adjustChannelVal = document.getElementById('adjustChannelVal');

const uploadButton = document.getElementById('uploadButton');
const uploadChannelNum = document.getElementById('uploadChannelNum');
const uploadChannelVal = document.getElementById('uploadChannelVal');
const uploadScript = document.getElementById('uploadScript');
const uploadReport = document.getElementById('uploadReport');

const baseUuid = '7d3a496c-7261-ae0f-a74b-9931dc80cc';

function hex2(x) {
	return Number(x).toString(16).padStart(2, '0');
}

function buf2hex(buffer) {
  return [...new Uint8Array(buffer)].map(hex2).join(' ');
}

function uuid_offset(x) {
	return baseUuid + hex2(x);
}

const serviceUuid = uuid_offset(0x00);
const statusChrcUuid = uuid_offset(0x01);
const controlChrcUuid = uuid_offset(0x02);
const uploadChrcUuid = uuid_offset(0x03);

function bufferToUTF8(x) {
	return new TextDecoder().decode(x);
}

function getU8(buf, pos) {
	return buf.getUint8(pos);
}

function getU16(buf, pos) {
	return buf.getUint16(pos, true);
}

function getU32(buf, pos) {
	return buf.getUint32(pos, true);
}

let device, statusChrc, controlChrc, uploadChrc;

const states = {
	0: "STOPPED",
	1: "UPLOADING",
	2: "RUNNING"
};

function updateStatusField(evt) {
	let buf = evt.target.value;

	console.log(buf);

	let text = buf2hex(buf.buffer)
	
	let numChannels = getU8(buf, 0);

	text += "\n" + numChannels + " channels:";

	for (let i = 0; i < numChannels; i++) {
		let off = 2 + (i * 12);
		let state = getU8(buf, off + 0);
		let adjust = getU16(buf, off + 2);
		let runtime_ms = getU32(buf, off + 4);
		let runcycles = getU32(buf, off + 8);
		text += "\n - Channel " + i + ": " + states[state] + ", adjust=" + adjust +", runtime=" + runtime_ms + "ms, cycles=" + runcycles + ", cycles/sec = " + Math.round(runcycles / (runtime_ms/1000)) + "Hz";
	}

	deviceState.innerText = text;
}

const ACTION_STOP = 0;
const ACTION_SET_ADJUST = 1;
const ACTION_START_UPLOAD = 2;
const ACTION_FINISH_UPLOAD = 3;

async function executeControlCommand(action, channel, adjust) {
	data = new Uint8Array([
		channel & 0xff, action & 0xff,
		adjust & 0xff, (adjust >> 8) & 0xff
	]);
	await controlChrc.writeValueWithResponse(data);
}

async function doUpload() {
	let channel = parseInt(uploadChannelNum.value);
	let adjust = parseInt(uploadChannelVal.value);
	let script = uploadScript.value;
	let mtu = 20;

	uploadReport.innerText = "Starting";

	await executeControlCommand(ACTION_START_UPLOAD, channel, adjust);

	chunks = Math.floor(script.length / mtu);
	console.log("script length=" + script.length + ", chunks = " + chunks);

	for (let i = 0; i < chunks; i++) {
		uploadReport.innerText = "Writing chunk " + i + " / " + chunks;
		chunk = script.substring(i * mtu, (i + 1) * mtu);
		console.log("Write chunk, len=" + chunk.length);
		await uploadChrc.writeValueWithResponse(
			new TextEncoder().encode(chunk));
	}

	leftover = script.substring(chunks * mtu);
	if (leftover.length != 0) {
		uploadReport.innerText = "Writing last chunk";
		console.log("Write last chunk, len=" + leftover.length);
		await uploadChrc.writeValueWithResponse(
			new TextEncoder().encode(leftover));
	}

	uploadReport.innerText = "Finishing";

	await executeControlCommand(ACTION_FINISH_UPLOAD, channel, adjust);

	uploadReport.innerText = "Done!";
}

queryButton.onclick = async () => statusChrc.readValue();
stopChannelButton.onclick = async () => await executeControlCommand(ACTION_STOP, parseInt(stopChannelNum.value), 0);
adjustChannelButton.onclick = async () => await executeControlCommand(ACTION_SET_ADJUST, parseInt(adjustChannelNum.value), parseInt(adjustChannelVal.value));
uploadButton.onclick = async () => await doUpload();

connectButton.onclick = async () => {
  device = await navigator.bluetooth
             .requestDevice({
                filters: [{
                  services: [serviceUuid]
                }]
             });
  const server = await device.gatt.connect();
  const service = await server.getPrimaryService(serviceUuid);
  statusChrc = await service.getCharacteristic(statusChrcUuid);
  controlChrc = await service.getCharacteristic(controlChrcUuid);
  uploadChrc = await service.getCharacteristic(uploadChrcUuid);

  statusChrc.addEventListener('characteristicvaluechanged', updateStatusField);
  statusChrc.readValue();
  
  device.ongattserverdisconnected = disconnect; 
  connectButton.disabled = true;
  disconnectButton.disabled = false;
  connectionState.innerText = "Connected";
};

const disconnect = () => {
  connectionState.innerText = "Disconnected";
  connectButton.disabled = false;
  disconnectButton.disabled = true;
};

disconnectButton.onclick = async () => {
  await device.gatt.disconnect();
  disconnect();
};

disconnect();

if (!navigator.bluetooth) {
	alert("Web Bluetooth not supported. Try Chrome?");
}

};