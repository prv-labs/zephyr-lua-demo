--sawtooth
function periodic(dt_ms, adjust)
	local fraction = (dt_ms % adjust) / adjust

	return fraction * 100
end

return periodic