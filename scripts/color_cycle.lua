--cycle
function periodic(dt_ms, adjust)
	local fraction = (dt_ms % adjust) / adjust

	if fraction > 0.66 then
		return {r = 100, g = 0, b = 0}
	elseif fraction > 0.33 then
		return {r = 0, g = 100, b = 0}
	else
		return {r = 0, g = 0, b = 100}
	end
end

return periodic