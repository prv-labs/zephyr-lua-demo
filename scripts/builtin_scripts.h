/*
 * Created by Louis Goessling on 2/22/22.
 *
 * Copyright (c) 2022 PRV Labs. All rights reserved.
 */

#ifndef BUILTIN_SCRIPTS_H
#define BUILTIN_SCRIPTS_H

#include "userland.h"

extern IN_USERLAND_INITIALIZED int	 builtin_scripts_count;
extern IN_USERLAND_INITIALIZED int	 builtin_scripts_lengths[];
extern IN_USERLAND_INITIALIZED char* builtin_scripts_texts[];
extern IN_USERLAND_INITIALIZED char* builtin_scripts_names[];

#endif	// BUILTIN_SCRIPTS_H
