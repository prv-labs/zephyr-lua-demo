--sine
function periodic(dt_ms, adjust)
	local fraction = (dt_ms % adjust) / adjust

	return 100 * math.sin(2 * math.pi * fraction)
end

return periodic