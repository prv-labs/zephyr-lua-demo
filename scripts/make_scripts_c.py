#!/usr/bin/env python3

import sys, os

_, scripts, output_path = sys.argv

scripts = scripts.split(';')

print("Embedding scripts in build:")
for name in scripts:
	print(" ->", name)

contents = []

for filename in scripts:
	with open(filename, 'r') as fd:
		contents.append(fd.read())

def cstringify(x):
	r = '"'
	for char in x:
		code = ord(char)
		hexnum = hex(code)[2:]
		if len(hexnum) == 1:
			hexnum = '0' + hexnum
		r += '\\x' + hexnum
	r += '\\x00"'
	return r

output = f"""
#include "builtin_scripts.h"

IN_USERLAND_INITIALIZED int   builtin_scripts_count     = {len(contents)};
IN_USERLAND_INITIALIZED int   builtin_scripts_lengths[] = {{ {",".join(str(len(x)) for x in contents)} }};
IN_USERLAND_INITIALIZED char* builtin_scripts_texts[]   = {{ {",".join(cstringify(x) for x in contents)} }};
IN_USERLAND_INITIALIZED char* builtin_scripts_names[]   = {{ {",".join(cstringify(x.split('/')[-1]) for x in scripts)} }};
"""

with open(output_path, 'w') as fd:
	fd.write(output)
