--square
function periodic(dt_ms, adjust)
	local fraction = (dt_ms % adjust) / adjust

	if fraction > 0.5 then
		return 100
	else
		return 0
	end
end

return periodic