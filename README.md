# zephyr-lua-demo

Lua-in-userspace demo for Zephyr Developer Summit 2022

## Quickstart

To flash this onto your nrf52840dongle, grab a precompiled copy of the app DFU package from the Discord, and run `nrfutil dfu usb-serial -pkg app.zip -p /dev/ttyACM0` (`COM1` instead of `ttyACM0` on Windows, I think) to flash it onto your dongle. Your dongle will pick a unique (but not very useful) Bluetooth name from it's hardware serial number.

Open (`Ctrl+O` in Chrome) `client/client.html` in this folder to get the control interface. There are example lua scripts in the `scripts/` folder that may be uploaded and run.

## Building

To hack on this on your own:

```
mkdir zephyr-lua-demo-workspace && cd zephyr-lua-demo-workspace
git clone https://gitlab.com/prv-labs/zephyr-lua-demo.git
west init -l zephyr-lua-demo && west update
west build -b nrf52840dongle_nrf52840 zephyr-lua-demo/
nrfutil pkg generate --hw-version 52 --sd-req=0x00 --application build/zephyr/zephyr.hex --application-version 1 app.zip
nrfutil dfu usb-serial -pkg app.zip -p /dev/ttyACM0
```

## Details

The API Lua scripts must follow is that they must return a function which takes two parameters, the first is time since starting in milliseconds, and the second is the opaque 'adjust' value set on the web UI (default 1000.) They must return either an integer between `0` and `100`, or a table containing keys `r`, `g`, and `b` corresponding to integers between `0` and `100`. Scripts may call `get_button()` to retrieve the state of the hardware button. Internally a script returning the integer `x` is mapped to the table `{r = x, g = b, b = x}`.

On the nrf52840dongle, channel 0 controls the RGB led, and channel 1 controls the green LED. On a nrf52840dk, channel 1 controls LEDs 1, 2, 3 and channel 2 controls LED 4. The non-rgb LED is controlled from the `g` intensity value of that channel's script.

