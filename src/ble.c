/*
 * Created by Louis Goessling on 6/3/22.
 *
 * Copyright (c) 2022 Entropic Engineering. All rights reserved.
 */

#include "userland.h"

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/gatt.h>
#include <bluetooth/uuid.h>
#include <drivers/hwinfo.h>
#include <settings/settings.h>
#include <stdio.h>
#include <zephyr.h>

#include <logging/log.h>

LOG_MODULE_REGISTER( ble, LOG_LEVEL_DBG );

#define BT_UUID_PLUS( x ) \
	BT_UUID_128_ENCODE( 0x7d3a496c, 0x7261, 0xae0f, 0xa74b, 0x9931dc80cc00 + x )
#define BT_UUID_PLUS_DECL( x ) BT_UUID_DECLARE_128( BT_UUID_PLUS( x ) )

const struct bt_data ad[] = {
	BT_DATA_BYTES( BT_DATA_FLAGS, ( BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR ) ),
	BT_DATA_BYTES( BT_DATA_UUID128_ALL, BT_UUID_PLUS( 0x00 ) ),
};

ssize_t tasks_state_read( struct bt_conn*			 conn,
						  struct bt_gatt_attr const* attr,
						  void*						 buf,
						  uint16_t					 len,
						  uint16_t					 offset )
{
	struct {
		uint8_t num_channels;
		uint8_t pad;
		struct entry {
			uint8_t state;
#define STATE_STOPPED	0
#define STATE_UPLOADING 1
#define STATE_RUNNING	2

			uint8_t	 pad;
			uint16_t adjust;
			uint32_t runtime_ms;
			uint32_t ran_cycles;
		} __packed states[USER_TASK_COUNT];
	} __packed message = { 0, .num_channels = USER_TASK_COUNT };

	for ( int i = 0; i < USER_TASK_COUNT; i++ )
	{
		struct entry* entry = &message.states[i];

		if ( user_tasks[i].tid == NULL )
		{
			entry->state = STATE_STOPPED;
		}
		else if ( user_tasks[i].started )
		{
			entry->state = STATE_RUNNING;
			entry->runtime_ms =
				( k_uptime_get() & 0xffffffff ) - user_tasks[i].start_ms;
			entry->ran_cycles = user_tasks_u[i].execution_cycle_count;
			entry->adjust	  = user_tasks_u[i].adjust_value;
		}
		else
		{
			entry->state = STATE_UPLOADING;
		}
	}

	return bt_gatt_attr_read( conn, attr, buf, len, offset, &message,
							  sizeof( message ) );
}

int selected_upload_channel = -1;

static ssize_t tasks_control_write( struct bt_conn*			   conn,
									struct bt_gatt_attr const* attr,
									void const*				   buf,
									uint16_t				   len,
									uint16_t				   offset,
									uint8_t					   flags )
{
	struct control_write {
		uint8_t channel;
		uint8_t action;
#define ACTION_STOP			 0
#define ACTION_SET_ADJUST	 1
#define ACTION_START_UPLOAD	 2
#define ACTION_FINISH_UPLOAD 3

		uint16_t adjust;
	} __packed;

	if ( offset != 0 )
	{
		LOG_WRN( "Reject control write: offset != 0" );
		return BT_GATT_ERR( BT_ATT_ERR_INVALID_OFFSET );
	}
	if ( len != sizeof( struct control_write ) )
	{
		LOG_WRN( "Reject control write: size wrong" );
		return BT_GATT_ERR( BT_ATT_ERR_INVALID_ATTRIBUTE_LEN );
	}

	struct control_write* message = (struct control_write*) buf;

	if ( message->channel >= USER_TASK_COUNT )
	{
		LOG_WRN( "Reject control write: channel invalid" );
		return BT_GATT_ERR( BT_ATT_ERR_VALUE_NOT_ALLOWED );
	}

	bool task_stopped = user_tasks[message->channel].tid == NULL;

	if ( message->action == ACTION_STOP )
	{
		if ( task_stopped )
		{
			LOG_WRN( "Reject control write: ACTION_STOP with stopped task" );
			return BT_GATT_ERR( BT_ATT_ERR_VALUE_NOT_ALLOWED );
		}

		user_task_abort( &user_tasks[message->channel] );
	}
	else if ( message->action == ACTION_SET_ADJUST )
	{
		if ( task_stopped )
		{
			LOG_WRN(
				"Reject control write: ACTION_SET_ADJUST with stopped task" );
			return BT_GATT_ERR( BT_ATT_ERR_VALUE_NOT_ALLOWED );
		}

		user_tasks_u[message->channel].adjust_value = message->adjust;
	}
	else if ( message->action == ACTION_START_UPLOAD )
	{
		if ( ! task_stopped )
		{
			LOG_DBG( "Stopping running task for ACTION_START_UPLOAD" );
			user_task_abort( &user_tasks[message->channel] );
		}

		user_task_init( message->channel );
		selected_upload_channel = message->channel;
	}
	else if ( message->action == ACTION_FINISH_UPLOAD )
	{
		if ( task_stopped || user_tasks[message->channel].started )
		{
			LOG_WRN( "Reject control write: ACTION_FINISH_UPLOAD with stopped "
					 "or started task" );
			return BT_GATT_ERR( BT_ATT_ERR_VALUE_NOT_ALLOWED );
		}

		user_tasks_u[message->channel].adjust_value = message->adjust;
		user_task_start( &user_tasks[message->channel] );
		selected_upload_channel = -1;
	}
	else
	{
		LOG_WRN( "Reject control write: unknown action" );
		return BT_GATT_ERR( BT_ATT_ERR_VALUE_NOT_ALLOWED );
	}

	return sizeof( struct control_write );
}

static ssize_t add_text_write( struct bt_conn*			  conn,
							   struct bt_gatt_attr const* attr,
							   void const*				  buf,
							   uint16_t					  len,
							   uint16_t					  offset,
							   uint8_t					  flags )
{
	if ( offset != 0 )
	{
		return BT_GATT_ERR( BT_ATT_ERR_INVALID_OFFSET );
	}
	if ( len == 0 )
	{
		return BT_GATT_ERR( BT_ATT_ERR_INVALID_ATTRIBUTE_LEN );
	}

	if ( user_task_add_text( &user_tasks[selected_upload_channel], buf, len ) !=
		 0 )
	{
		return BT_GATT_ERR( BT_ATT_ERR_VALUE_NOT_ALLOWED );
	}

	return len;
}

BT_GATT_SERVICE_DEFINE( cts_cvs,
						BT_GATT_PRIMARY_SERVICE( BT_UUID_PLUS_DECL( 0x00 ) ),
						BT_GATT_CHARACTERISTIC( BT_UUID_PLUS_DECL( 0x01 ),
												BT_GATT_CHRC_READ,
												BT_GATT_PERM_READ,
												tasks_state_read,
												NULL,
												NULL ),
						BT_GATT_CHARACTERISTIC( BT_UUID_PLUS_DECL( 0x02 ),
												BT_GATT_CHRC_WRITE,
												BT_GATT_PERM_WRITE,
												NULL,
												tasks_control_write,
												NULL ),
						BT_GATT_CHARACTERISTIC( BT_UUID_PLUS_DECL( 0x03 ),
												BT_GATT_CHRC_WRITE,
												BT_GATT_PERM_WRITE,
												NULL,
												add_text_write,
												NULL ), );

char bt_name_buf[CONFIG_BT_DEVICE_NAME_MAX];

void start_ble()
{
	// FIXME
	// bt_passkey_set(123456u);

	if ( bt_enable( NULL ) )
	{
		LOG_ERR( "bt_enable failed" );
		k_panic();
	}

#ifdef CONFIG_SETTINGS
	if ( settings_load() )
	{
		LOG_WRN( "settings_load failed" );
	}
#endif

	char* end =
		strncpy( bt_name_buf, CONFIG_BT_DEVICE_NAME, sizeof( bt_name_buf ) ) +
		strlen( CONFIG_BT_DEVICE_NAME );
	char	id_buf[64];
	ssize_t id_len = hwinfo_get_device_id( id_buf, sizeof( id_buf ) );
	if ( id_len > 0 )
	{
		for ( int i = 0; i < id_len; i++ )
		{
			if ( ( end - bt_name_buf ) > ( CONFIG_BT_DEVICE_NAME_MAX - 2 ) )
				break;

			end += sprintf( end, "%x", id_buf[i] );
		}
	}

	bt_set_name( bt_name_buf );

	if ( bt_le_adv_start( BT_LE_ADV_CONN_NAME, ad, ARRAY_SIZE( ad ), NULL, 0 ) )
	{
		LOG_ERR( "bt_le_adv_start failed" );
		k_panic();
	}
}