/*
 * Created by Louis Goessling on 5/26/22.
 *
 * Copyright (c) 2022 PRV Labs. All rights reserved.
 */

#ifndef USERLAND_H
#define USERLAND_H

#include "hardware.h"

#include <app_memory/app_memdomain.h>
#include <zephyr.h>

typedef struct user_task_u {
	uint8_t* source_code;
	size_t	 source_code_len;

	int		   execution_cycle_count;
	uint16_t   adjust_value;
	struct rgb output_state;
} user_task_u_t;

typedef struct user_task_k {
	k_tid_t			tid;
	bool			started;
	uint32_t		start_ms;
	struct k_thread thread;
} user_task_k_t;

#define USERLAND_PART userland_part

#define IN_USERLAND_BSS			K_APP_BMEM( USERLAND_PART )
#define IN_USERLAND_INITIALIZED K_APP_DMEM( USERLAND_PART )

#define USER_TASK_HEAP_SIZE	 ( 1 << 15 )
#define USER_TASK_STACK_SIZE ( 1 << 13 )
#define USER_TASK_PRIORITY	 1
#define USER_TASK_COUNT		 2

#define USERLAND_SIGNAL_THREAD_STACK_SIZE 2048
#define USERLAND_SIGNAL_THREAD_PRIORITY	  ( -1 )
#define TIMESLICE						  20  // ms
#define TIMESLICE_PRIO_LIMIT			  0

extern struct user_task_k user_tasks[USER_TASK_COUNT];
extern struct user_task_u user_tasks_u[USER_TASK_COUNT];
;
extern struct sys_heap user_task_memory_heaps[USER_TASK_COUNT];

static inline int task_index_u( user_task_u_t* task )
{
	return task - user_tasks_u;
}

static inline int task_index_k( user_task_k_t* task )
{
	return task - user_tasks;
}

static inline struct sys_heap* heap_for_task_u( user_task_u_t* task )
{
	return &user_task_memory_heaps[task_index_u( task )];
}

void		   userland_init();
user_task_k_t* user_task_init( int channel );
int	 user_task_add_text( user_task_k_t* task, const char* buf, size_t size );
void user_task_start( user_task_k_t* task );
void user_task_abort( user_task_k_t* task );

void userland_raise_signal( user_task_u_t* task );

#endif	// USERLAND_H
