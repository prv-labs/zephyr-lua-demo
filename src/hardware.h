/*
 * Created by Louis Goessling on 6/3/22.
 *
 * Copyright (c) 2022 Entropic Engineering. All rights reserved.
 */

#ifndef HARDWARE_H
#define HARDWARE_H

#include <lua.h>

struct rgb {
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

void set_hw_output( int ch, struct rgb value );
void update_polled_hwapi();
void setup_lua_state_hwapi( lua_State* L );
void init_hwapi();

#endif	// HARDWARE_H
