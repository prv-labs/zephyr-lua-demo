/*
 * Created by Louis Goessling on 6/1/22.
 *
 * Copyright (c) 2022 PRV Labs. All rights reserved.
 */

#include "lua_runner.h"

#include "design_by_contract.h"
#include "hardware.h"
#include "userland.h"

#include <lua.h>
#include <luacompat/util.h>
#include <lualib.h>

#include <lauxlib.h>

#include <logging/log.h>
LOG_MODULE_REGISTER( script_runner, LOG_LEVEL_DBG );
#include <stdlib.h>

void* lua_allocator( void* ud, void* ptr, size_t osize, size_t nsize )
{
	user_task_u_t* task = ( (user_task_u_t*) ud );
	char*		   res =
		sys_heap_allocator( heap_for_task_u( task ), ptr, osize, nsize );

	return res;
}

// Access field .fieldname of the top item on the stack, leaving field value at
// top and item it was accessed on below it.
void get_lua_field( lua_State* L, char* fieldname )
{
	lua_pushstring( L, fieldname );
	lua_gettable( L, -2 );
}

int get_lua_num_0_to_100( lua_State* L )
{
	float f = lua_tonumber( L, -1 );
	int	  n = MIN( 100, MAX( 0, (int) f ) );
	return n;
}

void run_script( lua_State* L, user_task_u_t* task, char* script_text )
{
	LOG_DBG( "run_script starting lua script beginning '%.*s'",
			 MIN( 10, strchr( script_text, '\n' ) - script_text ),
			 script_text );

	if ( luaL_loadstring( L, script_text ) != LUA_OK )
	{
		// Error message is on top of the stack
		LOG_ERR( "Loading chunk text failed" );
		LOG_ERR( " (error: '%s')", lua_tostring( L, -1 ) );

		return;
	}

	int err = lua_pcall( L, 0, 1, 0 );	// Call chunk, returning table
	if ( err != LUA_OK )
	{
		// Error message is on the top of the stack
		LOG_ERR( "Executing chunk failed" );
		LOG_ERR( " (error: '%s')", lua_tostring( L, -1 ) );

		return;
	}

	ASSERT( lua_isfunction( L, -1 ) );	// Top of stack must be function

	// Get a handle to the periodic function, which must be on the top of the
	// stack when this is called
	int periodic_idx = lua_gettop( L );

	task->execution_cycle_count = 0;

	uint32_t start_time = k_uptime_get_32();

	while ( 1 )
	{
		// Ensure we are not leaking items onto the stack
		ASSERT( lua_gettop( L ) == periodic_idx );

		uint32_t now = k_uptime_get_32();

		// Push a copy of the periodic function so that we can call it. The call
		// will consume the value.
		lua_pushvalue( L, periodic_idx );

		// Push dt
		lua_pushinteger( L, now - start_time );

		// Push input parameter
		lua_pushinteger( L, task->adjust_value );

		// Now call the periodic function.
		// arg 1: state
		// arg 2: Two inputs (dt, parameter)
		// arg 3: One output: hardware output
		// arg 4: 0 indicates no error handler function; error message will
		// be on the stack if it fails
		int err = lua_pcall( L, 2, 1, 0 );
		if ( err != LUA_OK )
		{
			// Error message is on the top of the stack
			LOG_ERR( "periodic function returned error, failure." );

			LOG_ERR( " (error: '%s')", lua_tostring( L, -1 ) );

			abort();
		}
		else
		{
			// Function and arguments have been popped, stack contains return
			// values.

			if ( lua_istable( L, -1 ) )
			{
				// rgb table
				get_lua_field( L, "r" );
				task->output_state.r = get_lua_num_0_to_100( L );
				lua_pop( L, 1 );

				get_lua_field( L, "g" );
				task->output_state.g = get_lua_num_0_to_100( L );
				lua_pop( L, 1 );

				get_lua_field( L, "b" );
				task->output_state.b = get_lua_num_0_to_100( L );
				lua_pop( L, 1 );
			}
			else
			{
				// single number
				int n				 = get_lua_num_0_to_100( L );
				task->output_state.r = n;
				task->output_state.g = n;
				task->output_state.b = n;
			}

			lua_pop( L, 1 );  // pop hw outputs
		}

		task->execution_cycle_count++;

		userland_raise_signal( task );
		k_yield();
	}
}

#include "builtin_scripts.h"

void lua_demo( struct user_task_u* task )
{
	lua_State* L = lua_newstate( lua_allocator, task );

	luaL_configurenewstate( L );
	luaL_openlibs( L );

	lua_pushglobaltable( L );
	setup_lua_state_hwapi( L );
	lua_pop( L, 1 );

	ASSERT( task->source_code != NULL );
	run_script( L, task, task->source_code );
}
