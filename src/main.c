/*
 * Created by Louis Goessling on 5/26/22.
 *
 * Copyright (c) 2022 PRV Labs. All rights reserved.
 */

#include "ble.h"
#include "builtin_scripts.h"
#include "hardware.h"
#include "userland.h"

#include <zephyr.h>

#include <logging/log.h>

LOG_MODULE_REGISTER( app, LOG_LEVEL_DBG );

#ifdef CONFIG_LOG
extern struct k_thread logging_thread;
// Need to increase prio so this actually runs instead of our lua tasks
//  eating all the CPU forever.
#endif

int find_script_by_name( char* name )
{
	for ( int i = 0; i < builtin_scripts_count; i++ )
	{
		if ( strcmp( builtin_scripts_names[i], name ) == 0 )
		{
			return i;
		}
	}

	LOG_ERR( "Can't find script" );
	while ( 1 ) k_panic();
}

void main( void )
{
#ifdef CONFIG_LOG
	k_thread_priority_set( &logging_thread, -3 );  // Hack! See above.
#endif

	LOG_INF( "Initialize hwapi and userland..." );

	init_hwapi();
	userland_init();

	LOG_INF( "Start bluetooth..." );

	start_ble();

	LOG_INF( "Start builtin scripts..." );

	char* scripts_to_start[USER_TASK_COUNT] = { "color_cycle.lua", "sine.lua" };

	for ( int i = 0; i < USER_TASK_COUNT; i++ )
	{
		LOG_DBG( "Start builtin script %s on channel %d", scripts_to_start[i],
				 i );
		user_task_k_t* task		  = user_task_init( i );
		int			   script_idx = find_script_by_name( scripts_to_start[i] );
		user_task_add_text( task, builtin_scripts_texts[script_idx],
							builtin_scripts_lengths[script_idx] );
		user_task_start( task );
	}

	LOG_INF( "Initialization done. Executing hwapi poll loop" );

	while ( 1 )
	{
		update_polled_hwapi();
		k_msleep( 100 );
	}
}
