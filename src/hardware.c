/*
 * Created by Louis Goessling on 6/3/22.
 *
 * Copyright (c) 2022 Entropic Engineering. All rights reserved.
 */

#include "userland.h"

#include <lua.h>
#include <lualib.h>

#include <drivers/gpio.h>
#include <drivers/led.h>
#include <zephyr.h>

#include <logging/log.h>
LOG_MODULE_REGISTER( hardware, LOG_LEVEL_DBG );

static const struct device* leds_dev = DEVICE_DT_GET( DT_PATH( pwm_leds ) );
static const struct gpio_dt_spec button =
	GPIO_DT_SPEC_GET( DT_ALIAS( sw0 ), gpios );

IN_USERLAND_BSS bool button_state_u;

static int lua_read_button_state( lua_State* L )
{
	lua_pushboolean( L, button_state_u );
	return 1;
}

void set_hw_output( int ch, struct rgb value )
{
	if ( ch == 0 )
	{
		// rgb
		led_set_brightness( leds_dev, 0, value.r );
		led_set_brightness( leds_dev, 1, value.g );
		led_set_brightness( leds_dev, 2, value.b );
	}
	else if ( ch == 1 )
	{
		// expose only the green component
		led_set_brightness( leds_dev, 3, value.g );
	}
	else
	{
		k_panic();
	}
}

void setup_lua_state_hwapi( lua_State* L )
{
	LOG_INF( "Installing app-local lua facility" );

	lua_pushcfunction( L, lua_read_button_state );
	lua_setfield( L, -2, "get_button" );
}

void init_hwapi()
{
	gpio_pin_configure_dt( &button, GPIO_INPUT );
}

void update_polled_hwapi()
{
	bool ok	   = true;
	bool state = gpio_pin_get_dt( &button );
	for ( int i = 0; i < 10; i++ )
	{
		if ( state != gpio_pin_get_dt( &button ) )
		{
			ok = false;
			break;
		}
	}

	if ( ok )
	{
		button_state_u = state;
	}
}
