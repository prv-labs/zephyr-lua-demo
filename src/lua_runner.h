/*
 * Created by Louis Goessling on 6/1/22.
 *
 * Copyright (c) 2022 Entropic Engineering. All rights reserved.
 */

#ifndef LUA_RUNNER_H
#define LUA_RUNNER_H

#include "userland.h"

void lua_demo( struct user_task_u* task );

#endif	// LUA_RUNNER_H
