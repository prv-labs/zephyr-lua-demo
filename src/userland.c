/*
 * Created by Louis Goessling on 5/26/22.
 *
 * Copyright (c) 2022 PRV Labs. All rights reserved.
 */

#include "userland.h"

#include "design_by_contract.h"
#include "hardware.h"
#include "lua_runner.h"

#include <logging/log.h>
LOG_MODULE_REGISTER( user_tasks, LOG_LEVEL_DBG );

K_APPMEM_PARTITION_DEFINE( USERLAND_PART );

IN_USERLAND_BSS uint8_t user_task_memory[USER_TASK_COUNT][USER_TASK_HEAP_SIZE];
IN_USERLAND_BSS struct sys_heap user_task_memory_heaps[USER_TASK_COUNT];

// Kernel-side task structures.
struct user_task_k user_tasks[USER_TASK_COUNT];

// Statically allocate user-side task structures, same indices and rules as
// kernel-side task structures.
// WARNING: the values in here should not be treated
// as authoritative by any kernel code since they could have been corrupted by a
// lua crash/exploit.
IN_USERLAND_BSS user_task_u_t user_tasks_u[USER_TASK_COUNT];

// Blocks of stack memory of user tasks.
K_THREAD_STACK_ARRAY_DEFINE( user_stacks,
							 USER_TASK_COUNT,
							 USER_TASK_STACK_SIZE );

void k_sys_fatal_error_handler( unsigned int reason, const z_arch_esf_t* esf )
{
	LOG_ERR( "k_sys_fatal_error_handler invoked." );

	uint8_t flags	 = k_current_get()->base.thread_state;
	bool	aborting = flags & _THREAD_ABORTING;
	bool	dead	 = flags & _THREAD_DEAD;

	if ( aborting || dead )
	{
		// See https://github.com/zephyrproject-rtos/zephyr/issues/42496
		LOG_ERR( "Livelock detected. Halt." );
		k_fatal_halt( reason );
	}

	if ( k_current_get()->base.user_options & K_USER )
	{
		LOG_WRN( "Userspace thread; aborting it." );
		bool found = false;
		for ( int i = 0; i < USER_TASK_COUNT; i++ )
		{
			if ( user_tasks[i].tid == k_current_get() )
			{
				user_task_abort( &user_tasks[i] );
				found = true;
				break;
			}
		}

		if ( ! found )
		{
			LOG_ERR( "Unknown userspace thread. Halt." );
			k_fatal_halt( reason );
		}

		return;
	}
	else
	{
		LOG_ERR( "Kernel thread. Halt." );
		k_fatal_halt( reason );
	}
}

struct k_poll_signal userland_signal;

void do_pending_update( int channel_number )
{
	// Update must be for a valid channel number
	ASSERT( channel_number >= 0 && channel_number < USER_TASK_COUNT );

	user_task_u_t* task_u = &user_tasks_u[channel_number];

	set_hw_output( channel_number, task_u->output_state );
}

void userland_message_handler_loop( void* p1, void* p2, void* p3 )
{
	(void) p1;
	(void) p2;
	(void) p3;

	struct k_poll_event events[1] = {
		K_POLL_EVENT_INITIALIZER( K_POLL_TYPE_SIGNAL, K_POLL_MODE_NOTIFY_ONLY,
								  &userland_signal ),
	};

	for ( ;; )
	{
		int err = k_poll( events, 1, K_FOREVER );

		ASSERT( err == 0 );
		ASSERT( events[0].signal->signaled );

		int channel_number = events[0].signal->result;

		do_pending_update( channel_number );

		events[0].signal->signaled = 0;
		events[0].state			   = K_POLL_STATE_NOT_READY;

		k_sched_time_slice_set( TIMESLICE, TIMESLICE_PRIO_LIMIT );
	}
}

K_THREAD_STACK_DEFINE( userland_signal_thread_stack,
					   USERLAND_SIGNAL_THREAD_STACK_SIZE );
struct k_thread userland_signal_thread;

void userland_raise_signal( user_task_u_t* task )
{
	k_poll_signal_raise( &userland_signal, task - user_tasks_u );
}

struct k_mem_domain userland_domain;

void userland_init()
{
	k_mem_domain_init( &userland_domain, 0, NULL );
	k_mem_domain_add_partition( &userland_domain, &USERLAND_PART );

	k_poll_signal_init( &userland_signal );
	k_thread_create( &userland_signal_thread, userland_signal_thread_stack,
					 USERLAND_SIGNAL_THREAD_STACK_SIZE,
					 userland_message_handler_loop, NULL, NULL, NULL,
					 USERLAND_SIGNAL_THREAD_PRIORITY, 0, K_NO_WAIT );

	k_sched_time_slice_set( TIMESLICE, TIMESLICE_PRIO_LIMIT );
}

void user_entry_point( void* arg1, void* arg2, void* arg3 )
{
	(void) arg2;
	(void) arg3;

	user_task_u_t* task		 = (user_task_u_t*) arg1;
	char		   name[]	 = "user_task[X]\0";
	name[sizeof( name ) - 4] = '0' + task_index_u( task );

	k_thread_name_set( NULL, name );
	LOG_DBG( "Started %s", name );

	lua_demo( task );

	return;
}

user_task_k_t* user_task_init( int channel )
{
	user_task_k_t* task = &user_tasks[channel];

	// Make sure the task is not running right now
	ASSERT( task->tid == NULL );

	user_task_u_t* task_u = &user_tasks_u[channel];
	memset( task_u, 0, sizeof( user_task_u_t ) );
	task_u->adjust_value = 1000;  // default value

	memset( user_task_memory[channel], 0, USER_TASK_HEAP_SIZE );
	sys_heap_init( &user_task_memory_heaps[channel], user_task_memory[channel],
				   USER_TASK_HEAP_SIZE );

	task->tid =
		k_thread_create( &task->thread, user_stacks[channel],
						 USER_TASK_STACK_SIZE, user_entry_point, task_u, NULL,
						 NULL, USER_TASK_PRIORITY, K_USER, K_FOREVER );
	task->started = false;

	k_mem_domain_add_thread( &userland_domain, task->tid );

	k_object_access_grant( &userland_signal, task->tid );

	return task;
}

int user_task_add_text( user_task_k_t* task, const char* buf, size_t size )
{
	ASSERT(
		! task->started );	// Not safe to call sys_heap_xxx if it has ever run

	user_task_u_t* task_u = &user_tasks_u[task_index_k( task )];
	task_u->source_code	  = sys_heap_realloc(
		  &user_task_memory_heaps[task_index_k( task )], task_u->source_code,
		  task_u->source_code_len + size + 1 );
	if ( task_u->source_code == NULL )
	{
		LOG_WRN( "Out of space adding text to task" );
		return 1;
	}

	memcpy( task_u->source_code + task_u->source_code_len, buf, size );
	task_u->source_code[task_u->source_code_len + size] = 0;
	task_u->source_code_len += size;

	return 0;
}

void user_task_start( user_task_k_t* task )
{
	ASSERT( task->tid != NULL );

	task->started  = true;
	task->start_ms = k_uptime_get() & 0xffffffff;
	k_thread_start( task->tid );
}

void user_task_abort( user_task_k_t* task )
{
	ASSERT( task->tid != NULL );

	if ( task->started )
	{
		k_thread_abort( task->tid );
		task->tid	  = NULL;
		task->started = false;
	}

	struct rgb zero = { 0 };

	set_hw_output( task_index_k( task ), zero );
}
