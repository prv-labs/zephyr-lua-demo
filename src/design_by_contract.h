/*
 * Created by Benjamin Riggs on 1/26/22.
 *
 * Copyright (c) 2022 Entropic Engineering. All rights reserved.
 */

#ifndef DESIGN_BY_CONTRACT_H
#define DESIGN_BY_CONTRACT_H

#include <sys/__assert.h>

#ifdef CONFIG_ZTEST

#include <ztest.h>

#define ASSERT( value ) \
	zassert( value, "\n     -> ASSERTion that `" #value "` failed", NULL )

#else  // CONFIG_ZTEST

#ifdef CONFIG_LOG
#include <logging/log_ctrl.h>
#define REPORT_ERROR_IF_LOG_ENABLED( assertion )   \
	LOG_ERR( "ASSERTion `" assertion "` failed" ); \
	LOG_PANIC();
#else  // CONFIG_LOG
#define REPORT_ERROR_IF_LOG_ENABLED( assertion )
#endif	// CONFIG_LOG

#define ASSERT( value )                            \
	{                                              \
		if ( ! ( value ) )                         \
		{                                          \
			REPORT_ERROR_IF_LOG_ENABLED( #value ); \
			k_oops();                              \
		}                                          \
	}

#endif	// CONFIG_ZTEST

// Defining and asserting on a variable with a useful name indicates in the
// error message that it was an ASSERT_UNREACHABLE

#define ASSERT_UNREACHABLE()                   \
	{                                          \
		bool this_code_will_never_run = false; \
		ASSERT( this_code_will_never_run );    \
		while ( 1 ) { k_oops(); }              \
	}

#define ASSERT_FAIL ASSERT_UNREACHABLE

#endif	// DESIGN_BY_CONTRACT_H
