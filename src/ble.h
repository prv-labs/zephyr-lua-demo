/*
 * Created by Louis Goessling on 6/3/22.
 *
 * Copyright (c) 2022 Entropic Engineering. All rights reserved.
 */

#ifndef BLE_H
#define BLE_H

void start_ble();

#endif	// BLE_H
