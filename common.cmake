cmake_minimum_required(VERSION 3.20)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/..)

# Enable 'boards' directory in each project
list(APPEND BOARD_ROOT ${CMAKE_CURRENT_LIST_DIR})

# Workspace is a useful thing to have around
function(workspace)
    file(REAL_PATH ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/../ WRKSPC)
    set(WORKSPACE ${WRKSPC} PARENT_SCOPE)
endfunction()
# Need to wrap in a function to ensure path is taken relative to this file.
workspace()

IF(NOT DEFINED ENV{ZEPHYR_BASE})
    message(Setting ZEPHYR_BASE to WORKSPACE/zephyr)
    set(ENV{ZEPHYR_BASE} ${WORKSPACE}/zephyr)
ENDIF()

# Invoke Zephyr
find_package(Zephyr HINTS $ENV{ZEPHYR_BASE})

project(zephyr-lua-demo)

# CONFIGURE_DEPENDS on the folder ensures adding/deleting scripts is picked up
set_property(DIRECTORY APPEND PROPERTY CMAKE_CONFIGURE_DEPENDS scripts/)

# Collect all lua files. DEPEND on this below ensures script modification is picked up
file(GLOB LUA_FILES ${CMAKE_CURRENT_LIST_DIR}/scripts/*.lua)

add_custom_command(
  OUTPUT builtin_scripts.c
  COMMAND ${CMAKE_CURRENT_LIST_DIR}/scripts/make_scripts_c.py
                "${LUA_FILES}"
                builtin_scripts.c
  DEPENDS ${LUA_FILES}
  VERBATIM)

target_sources(app PRIVATE
    builtin_scripts.c
)

target_sources(app PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/src/userland.c
        ${CMAKE_CURRENT_LIST_DIR}/src/lua_runner.c
        ${CMAKE_CURRENT_LIST_DIR}/src/ble.c
)

target_include_directories(app PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/src/
        ${CMAKE_CURRENT_LIST_DIR}/scripts/
)
